## Books server

Server that generates a list of books and serves the list.

### How to run
compile source with typescript and run the server as `node server.js`. The server starts at port _3000_

#### Run with docker 

From root directory run _docker build -t example/book-list-server ._ in order to build the docker container and then start it running _docker run -it --rm -p 3000:3000 example/book-list-server_

#### Compiling typescript.
    - There is vscode watchifying task that can be executed.
    - From commend line using tsc `tsc -p .`