import * as moment from 'moment';
import * as fs from 'fs';

export class BooksGenerator {

    public static genders: Gender[] = ['male', 'female'];
    public static genres: Genre[] = ['Satire', 'Science fiction', 'Drama', 'Action', 'Romance', 'Mystery', 'Horror'];
    public static letters: string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'l', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

     /**
     * Generates a number of random books
     * 
     * @param numberOfBooks number of random books to generate.
     */
    public static generate(numberOfBooks: number): Book[] {
        const books: Book[] = [];
        books.push({
            name: 'hallowen_book',
            author: { name: 'hal_auth_', gender: 'male' },
            genre: 'Horror',
            published: moment('2015-10-31 12:00').utc().toDate()
        });
        books.push({
            name: 'finance_book',
            author: { name: 'finance_auth_august', gender: 'male' },
            genre: 'Finance',
            published: moment('2017-08-25 12:00').utc().toDate()
        });
        books.push({
            name: 'finance_book',
            author: { name: 'finance_auth_feb', gender: 'female' },
            genre: 'Finance',
            published: moment('2017-01-27 12:00').utc().toDate()
        });
        for (var i = 0; i < numberOfBooks; i++) {
            books.push({
                name: this.letters[i % 24] + this.letters[i % 4] + 'name_book_' + i,
                author: { name: this.letters[i % 7] + this.letters[i % 24] +'name_auth_' + i, gender: BooksGenerator.genders[i % 1] },
                genre: BooksGenerator.genres[i % 7],
                published: moment().utc().add(-i % 1000, 'days').toDate()
            });
        }
        return books;
    }
}

export default BooksGenerator;
