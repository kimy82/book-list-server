import * as http from 'http';
import * as fs from 'fs';
import * as moment from 'moment';
import BooksGenerator  from './booksGenerator';

const port = 3000;

const books: string = JSON.stringify(BooksGenerator.generate(1000000));

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Content-Type', 'text/html');
    res.write(books);
    res.end();
});

server.listen(port, () => {
    console.log(`Server running at http://hostname:${port}/`);
});
