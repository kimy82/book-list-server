FROM ubuntu:16.04
#docker build -t example/book-list-server .
#docker run -it --rm -p 3000:3000 example/book-list-server

USER root

#Install curl
RUN apt-get update \
    && apt-get -y install curl

#Install sudo command
RUN apt-get update && \
      apt-get -y install sudo

RUN mkdir /home/app/
WORKDIR /home/app/

#Install node and angular cli
RUN curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
RUN apt-get update && apt-get install -y nodejs
RUN npm install -g typescript

COPY typings /home/app/typings
COPY package.json /home/app/package.json
COPY tsconfig.json /home/app/tsconfig.json
COPY server.ts /home/app/server.ts
COPY booksGenerator.ts /home/app/booksGenerator.ts
RUN npm install
RUN tsc -p .
EXPOSE 3000
CMD ["node", "server.js"]
