type Gender = 'male' | 'female';
type Genre = 'Satire' | 'Science fiction' | 'Drama' | 'Action' | 'Romance' | 'Mystery' | 'Horror' | 'Finance';

interface Author {
    gender: Gender;
    name: string;
}

interface Book {
    name: string;
    author: Author;
    genre: Genre;
    published: Date;
}